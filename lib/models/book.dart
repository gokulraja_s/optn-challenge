import 'package:flutter/foundation.dart';

class Book {
  final String title;
  final String serialNumber;
  final int cost;
  Book({
    @required this.title,
    @required this.serialNumber,
    @required this.cost,
  });
}