import 'dart:async';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapSection extends StatefulWidget {
  @override
  _MapSectionState createState() => _MapSectionState();
}

class _MapSectionState extends State<MapSection> {
  //pointer
  bool showMyLocation = false;

  //location service and subscribe to location stream
  Location locationService;
  StreamSubscription<LocationData> locationSubscription;

  //camera controller and initial position
  Completer<GoogleMapController> myController = Completer();
  static final CameraPosition indiaCameraPosition = CameraPosition(
    target: LatLng(20.5937, 78.9629),
    zoom: 5,
  );

  //get permission and listen to location stream
  //change camera position to new location
  trackMyLocation() async {
    locationService = Location();
    try {
      bool serviceAccess = await locationService.serviceEnabled();
      if (serviceAccess) {
        bool locationPermission = await locationService.requestPermission();
        if (locationPermission) {
          locationSubscription = locationService.onLocationChanged().listen(
            (LocationData currentLocation) async {
              print(currentLocation.latitude);
              print(currentLocation.longitude);

              final GoogleMapController controller = await myController.future;
              controller.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(
                    target: LatLng(
                        currentLocation.latitude, currentLocation.longitude),
                    zoom: 15,
                  ),
                ),
              );
            },
          );
          setState(
            () {
              showMyLocation = true;
            },
          );
        }
      } else {
        //no access to location
        bool serviceStatusResult = await locationService.requestService();
        if (serviceStatusResult) {
          trackMyLocation();
        }
      }
    } on PlatformException catch (_) {
      //error
      print('error');
      locationService = null;
    }
  }

  @override
  void initState() {
    super.initState();
    trackMyLocation();
  }

  @override
  void dispose() {
    super.dispose();
    locationSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      initialCameraPosition: indiaCameraPosition,
      mapType: MapType.normal,
      myLocationEnabled: showMyLocation,
      myLocationButtonEnabled: false,
      onMapCreated: (GoogleMapController controller) {
        myController.complete(controller);
      },
    );
  }
}
