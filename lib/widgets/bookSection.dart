import 'package:flutter/material.dart';
import 'package:livelocation/models/book.dart';
import 'package:livelocation/providers/bookProvider.dart';
import 'package:provider/provider.dart';

class BookSection extends StatefulWidget {
  @override
  _BookSectionState createState() => _BookSectionState();
}

class _BookSectionState extends State<BookSection> {
  @override
  Widget build(BuildContext context) {
    List<Book> books = Provider.of<BookProvider>(context).books;
    return ListView.builder(
      itemCount: books.length,
      itemBuilder: (context,index) {
        return ListTile(
          leading: CircleAvatar(
            child: FittedBox(
              child: Text(
                books[index].cost.toString()
              ),
            ),
          ),
          title: Text(
            books[index].title,
            style: TextStyle(
              fontSize: 18,
            ),
          ),
          subtitle: Text(
            books[index].serialNumber,
          ),
        );
      },
    );
  }
}
