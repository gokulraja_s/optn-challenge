import 'package:flutter/material.dart';
import 'package:livelocation/providers/bookProvider.dart';
import 'package:livelocation/widgets/bookSection.dart';
import 'package:livelocation/widgets/mapSection.dart';
import 'package:provider/provider.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'OptN',
      home: Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 7,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20.0),
                    bottomRight: Radius.circular(20.0),
                  ),
                  child: MapSection(),
                ),
              ),
              Expanded(
                flex: 4,
                child: ChangeNotifierProvider.value(
                  value: BookProvider(),
                  child: Container(
                    child: BookSection(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
