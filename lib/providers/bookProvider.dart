import 'package:flutter/foundation.dart';
import 'package:livelocation/models/book.dart';

class BookProvider with ChangeNotifier {
  final List<Book> books = [
    Book(
      title: 'Theory of everything',
      serialNumber: 'DGDHJSKSLLR',
      cost: 1200,
    ),
    Book(
      title: 'Learn japanese in 100 days',
      serialNumber: 'FHSDJKSDLSD',
      cost: 500,
    ),
    Book(
      title: 'Bachelors receipe book',
      serialNumber: 'NKVKENKSKK',
      cost: 200,
    ),
    Book(
      title: 'Indian economy',
      serialNumber: 'NFJDCBKFMML',
      cost: 750,
    ),
  ];
}
